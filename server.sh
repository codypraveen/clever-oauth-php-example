#!/bin/bash
# The url is same as CLEVER_REDIRECT_BASE in config.php
# Make sure port 9000 is not blocked by your firewall

echo -e "\e[96m Start clever demo server \e[39m"

php -S localhost:9000 index.php
