<?php
// Obtain your Client ID and secret from your Clever developer dashboard
// at https://account.clever.com/partner/applications

return array(
    'CLEVER_CLIENT_ID' => 'PASTE_ID_HERE',
    'CLEVER_CLIENT_SECRET' => 'PASTE_SECRET_HERE',
    'CLEVER_REDIRECT_BASE' => 'http://localhost:9000'
);